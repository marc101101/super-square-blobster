var platforms,
    movablePlatforms,
    player,
    cursors,
    gravity,
    death,
    score,
    stars,
    starsText,
    starIcon,
    scoreText,
    timeText,
    collectibles,
    marginScore,
    goal,
    spikes,
    pose,
    died,
    sawblades,
    playerVelocityX,
    playerVelocityY,
    worldWidth,
    worldHeight,
    backgroundWidth,
    backgroundHeight,
    shield,
    hitDeath,
    enemy,
    enemies,
    powerupSpeed,
    touchingCollectibles,
    highscoreCounter,
    pad1;

var levelTwo = function () {
    "use strict";
    platforms = null;
    movablePlatforms = null;
    player = null;
    cursors = null;
    gravity = 600;
    death = null;
    score = 0;
    stars = 0;
    starsText = null;
    starIcon = null;
    scoreText = null;
    timeText = null;
    collectibles = null;
    marginScore = 16;
    goal = null;
    spikes = null;
    pose = false;
    died = false;
    sawblades = null;
    playerVelocityX = 250;
    playerVelocityY = 350;
    worldWidth = 6000;
    worldHeight = 600;
    backgroundWidth = 800;
    backgroundHeight = 600;
    shield = false;
    hitDeath = false;
    enemy = null;
    enemies = null;
    powerupSpeed = false;
    touchingCollectibles = false;
    highscoreCounter = 0;
    pad1 = null;
};

levelTwo.prototype = {
    /* global Phaser
    global sessionStorage
    global localStorage */

    /* Function that loads all the assets, adds arcade physics, and initializes the groups as well as the UI. */
    create: function () {
        "use strict";
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.game.world.setBounds(0, 0, worldWidth + 1200, worldHeight);

        this.createBackground();
        this.initGroups();
        this.initUI();

        this.addGround(-75, "single");
        this.addGround(898, "single");
        this.addGround(1685, "left");
        this.addGround(2040, "middle");
        this.addGround(2390, "right");
        this.addGround(3635, "higher");
        this.addGround(5000, "high");
        this.addGround(5500, "higher");

        this.addPlatform(400, 400, "big", true);
        this.addPlatform(1025, 360, "small", false);
        this.addPlatform(1375, 375, "medium", false);
        this.addPlatform(2150, 375, "small", true);
        this.addPlatform(2790, 410, "small", true);
        this.addPlatform(2915, 310, "small", false);
        this.addPlatform(3050, 210, "small", true);
        this.addPlatform(3225, 310, "small", true);
        this.addPlatform(3400, 220, "small", false);
        this.addPlatform(4600, 300, "medium", true);
        this.addPlatform(5300, 250, "small", true);

        this.addSawblade(305, 400);
        this.addSawblade(550, 320);
        this.addSawblade(805, 400);
        this.addSawblade(1050, 425);
        this.addSawblade(1775, 435);
        this.addSawblade(1950, 435);
        this.addSawblade(2370, 435);
        this.addSawblade(2550, 435);
        this.addSawblade(3600, 255);
        this.addSawblade(5150, 295);

        this.addStars(235, 380, 2, 1, 2);
        this.addStars(435, 340, 2, 1, 0);
        this.addStars(870, 340, 2, 1, 1);
        this.addStars(1068, 150, 1, 1, 0);
        this.addStars(1525, 435, 2, 1, 0);
        this.addStars(1880, 445, 1, 1, 0);
        this.addStars(2150, 445, 3, 1, 0);
        this.addStars(2480, 445, 1, 1, 0);
        this.addStars(2956, 255, 1, 1, 0);
        this.addStars(3094, 160, 1, 1, 0);
        this.addStars(3447, 170, 1, 1, 0);
        this.addStars(3900, 100, 4, 4, 0);
        this.addStars(4400, 180, 2, 1, 2);
        this.addStars(4485, 142, 2, 1, 1);
        this.addStars(5105, 250, 2, 1, 2);
        this.addStars(5190, 212, 2, 1, 1);
        this.addStars(5920, 100, 1, 1, 0);
        this.addStars(5170, 100, 1, 1, 0);
        this.addStars(5920, 100, 1, 0, 0);

        this.addSpikePit(280, 6);
        this.addSpikePit(1250, 4);
        this.addSpikePit(2745, 9);
        this.addSpikePit(4300, 7);

        this.addGoal(5900, 260);

        this.addPowerUp(2187, 310, "shield");
        this.addPowerUp(660, 335, "speed");

        this.addEnemy(1750, 432);
        this.addEnemy(3715, 234);

        this.initPlayer();

        this.game.time.events.add(Phaser.Timer.MINUTE * 3, this.collision, this);
        touchingCollectibles = false;
        score = 0;
        stars = 0;
        highscoreCounter = 0;
        this.game.input.gamepad.start();
        pad1 = this.game.input.gamepad.pad1;

    },
    /* Function that is repeatedly called when the game is running. It is responsible for showing animations of the assets as well as the character.
    Moreover, it checks for cheats, updates the UI, and tests whether the character collides with enemies, collectibles, or traps. */
    update: function () {
        "use strict";
        if (!this.game.isPaused) {

            this.game.background.position.x = -(this.game.camera.x * 0.001);

            this.updateCollisions();

            this.checkForCheats();

            this.rotateSawblades();

            this.updatePlayerMovement();
            this.exitLevel();
            timeText.text = this.millisToMinutesAndSeconds(this.game.time.events.duration);

            this.moveEnemies(0, 2150, 1);
            this.moveEnemies(1, 3900, 1);
        }
    }
};

/* Creates the background according to the width and the height defined above. */
levelTwo.prototype.createBackground = function () {
    "use strict";
    for (var i = 0; i < (worldWidth / backgroundHeight).toFixed(0); i++) {
        this.game.background = this.game.add.tileSprite(0, 0, backgroundWidth + i * backgroundWidth, backgroundHeight, "bg2");
    }
    this.game.background.fixedToCamera = false;
};

/* Initializes the user interface, that is to say the texts and icons that inform the user about the time and the collectibles he has collected. */
levelTwo.prototype.initUI = function () {
    "use strict";
    scoreText = this.game.add.text(marginScore, marginScore, "000000", {
        fontSize: "24px",
        fill: "#FFF",
        stroke: "#000",
        strokeThickness: 8
    });
    scoreText.fixedToCamera = true;

    starsText = this.game.add.text(350, marginScore, "000", {
        fontSize: "24px",
        fill: "#FFF",
        stroke: "#000",
        strokeThickness: 8
    });
    starsText.fixedToCamera = true;

    starIcon = this.game.add.sprite(410, marginScore + 5, "star");
    starIcon.scale.setTo(0.8, 0.8);
    starIcon.fixedToCamera = true;

    timeText = this.game.add.text(700, marginScore, "3:00", {
        fontSize: "24px",
        fill: "#FFF",
        stroke: "#000",
        strokeThickness: 8
    });
    timeText.fixedToCamera = true;

};

/* Animation that is called when the user has either touched an enemy or a trap. It tests whether the player has collected the shield power up.
After the animation has been played, the level is restarted. */
levelTwo.prototype.collision = function () {
    "use strict";
    if (!shield) {
        died = true;
        player.body.collideWorldBounds = false;
        player.body.velocity.x = 0;
        player.body.velocity.y = 0;
        player.body.velocity.y = -400;
        this.game.time.events.add(Phaser.Timer.SECOND * 2.5, this.restartGame, this);
    } else {
        hitDeath = true;
        this.game.time.events.add(Phaser.Timer.SECOND, this.setShieldBackToFalse, this);
    }
};

/* Sets the shield boolean value back to false. This function is called when the user, having collected the hsiled beforehand, has touched either an enemy or a trap. */
levelTwo.prototype.setShieldBackToFalse = function () {
    "use strict";
    shield = false;
    hitDeath = false;
};

/* Restarts the game by resetting all significant variables to their default value. */
levelTwo.prototype.restartGame = function () {
    "use strict";
    score = 0;
    stars = 0;
    pose = false;
    died = false;
    this.game.isPaused = false;
    this.endPowerUp(0);
    this.game.state.start(this.game.state.current);
};

/**
Adds an enemy to the game including left and right walking animations.
@param {int} xPosition - the enemy's x coordniate
@param {int} yPosition - the enemy's y coordniate
*/
levelTwo.prototype.addEnemy = function (xPosition, yPosition) {
    "use strict";
    enemy = enemies.create(xPosition, yPosition, "enemy");
    enemy.body.immovable = true;
    enemy.animations.add("left", [0, 1], 5, true);
    enemy.animations.add("right", [3, 4], 5, true);
};

/**
Function that moves one of the movable platforms. It needs the number of the platform (starting at 0), its direction (either "x" or "y"),
its border (at which it will turn the other way), and its speed.
@param {int} number - number of the platform
@param {String} direction - direction in which the platform moves
@param {int} border - the coordinate value at which the platform stops to move and turns the other way
@param {int} start - the coordinate value where the platform starts
@param {int} speed - the platform's speed
*/
levelTwo.prototype.movePlatform = function (number, direction, border, start, speed) {
    "use strict";
    var platform = movablePlatforms.getAt(number);
    switch (direction) {
    case "x":
        platform.body.velocity.x = speed;
        if (platform.x > border) {
            speed = -speed;
        }
        break;
    case "y":
        if (platform.y < border) {
            platform.y += speed;
        } else {
            platform.y -= speed;
        }
        break;
    }
};

/**
Function which makes enemies move.
@param {int} number - number of the enemy
@param {int} border - the coordinate value at which the enemy stops to move and turns the other way
@param {int} speed - the enemy's speed
*/
levelTwo.prototype.moveEnemies = function (number, border, speed) {
    "use strict";
    var enemyCurrent = enemies.getAt(number);
    if (enemyCurrent.x < border) {
        enemyCurrent.body.velocity.x += speed;
    } else {
        enemyCurrent.body.velocity.x += -speed;
    }
    if (enemyCurrent.body.velocity.x < 0) {
        enemyCurrent.animations.play("left");
    } else {
        enemyCurrent.animations.play("right");
    }

};

/* Function that sets touchingCollectibles back to false which gives the character the ability to jump again, right after having collected a collectible. */
levelTwo.prototype.setTouchingCollectiblesBackToFalse = function () {
    "use strict";
    touchingCollectibles = false;
};

/**
Function that is called when the player collects a collectible. It checks whether the collectible is a star, a power up, or the goal. In the former case, it updates the UI respectively and kills the coin. In the case of the goal, this function does nothing. If the collectible is a power up - speed or shield - respective methods are called. By setting touchingCollectibles to true, this function keeps the character from being able to jump when touching the collectibles.
@param {Object} playerCurrent - the character touching the collectible
@param {Object} coin - the collectible touched by the character
*/
levelTwo.prototype.collectCoin = function (playerCurrent, coin) {
    "use strict";
    touchingCollectibles = true;
    if (coin.name === "speed") {
        coin.kill();
        this.startPowerUp(0);
        this.game.time.events.add(Phaser.Timer.SECOND * 0.001, this.setTouchingCollectiblesBackToFalse, this);
    } else if (coin.name === "shield") {
        coin.kill();
        shield = true;
        this.game.time.events.add(Phaser.Timer.SECOND * 0.001, this.setTouchingCollectiblesBackToFalse, this);
    } else if (coin.name !== "goal") {
        coin.kill();
        stars++;
        starsText.text = this.pad(stars, 3);
        score += 10;
        scoreText.text = this.pad(score, 6);
        this.game.time.events.add(Phaser.Timer.SECOND * 0.001, this.setTouchingCollectiblesBackToFalse, this);
    }
};

/**
Function that helps with the calculation of the score.
@param {int} num - number of stars or score respectively
@param {int} size - the number of digits being displayed in the UI
@return {String} - the respective UI element
*/
levelTwo.prototype.pad = function (num, size) {
    "use strict";
    var s = "000000" + num;
    return s.substr(s.length - size);
};

/**
Function that formats the timer.
@param {int} millis - milliseconds which need to be formated
@return {String} - the respective UI element
*/
levelTwo.prototype.millisToMinutesAndSeconds = function (millis) {
    "use strict";
    var minutes = Math.floor(millis / 60000);
    var seconds = ((millis % 60000) / 1000).toFixed(0);
    return minutes + ":" + (seconds < 10 ? "0" : "") + seconds;
};

/* Function that is called when the user finishes the level, it sets explicit varaibles to the needed values, such as pose to true,
which triggers the respective pose animation. It also calls the function which calculates the highscore. */
levelTwo.prototype.finishLevel = function () {
    "use strict";
    player.body.velocity.x = 0;
    pose = true;
    goal.frame = 0;
    this.game.time.events.add(Phaser.Timer.SECOND * 5, this.changeLevel, this);
    if (highscoreCounter === 0) {
        this.setHighscore(scoreText.text, timeText.text);
        highscoreCounter++;
    }
};

/** Function that calculates the highscore and saves the user's current score to the browser's localStorage object.
@param {String} scoreCurrent - the user's final score after having finished the level
@param {String} time - the time the user need to complete the level
*/
levelTwo.prototype.setHighscore = function (scoreCurrent, time) {
    "use strict";
    var highScoreObject,
        username;
    time = (parseInt(time.substring(0, 1)) * 60) + parseInt(time.substring(2));
    username = sessionStorage.getItem("username");
    highScoreObject = {
        "time": time,
        "username": username,
        "score": scoreCurrent,
        "accumulatedScore": parseInt(score) + time,
        "level": 2
    };
    this.sendScoreToScreen(highScoreObject);
    highScoreObject = JSON.stringify(highScoreObject);
    localStorage.setItem("highScoreEntry" + localStorage.length, highScoreObject);
};

/**
Function that sends information about the user's score to screen. It displays the number of stars the user collected, the time left, and the accumulated score.
@param {Object} highScoreObject - object that contains all the important score information
*/
levelTwo.prototype.sendScoreToScreen = function (highScoreObject) {
    "use strict";
    this.game.add.image(this.game.camera.x + 200, 100, "highScoreLevelEnd");
    this.game.add.text(this.game.camera.x + 350, 176, (String(highScoreObject.score)).substring(3, 5), {
        fontSize: "48px",
        fill: "#fbb03b",
        stroke: "#000",
        strokeThickness: 8
    });
    this.game.add.text(this.game.camera.x + 350, 246, highScoreObject.time, {
        fontSize: "48px",
        fill: "#fbb03b",
        stroke: "#000",
        strokeThickness: 8
    });
    this.game.add.text(this.game.camera.x + 350, 316, highScoreObject.accumulatedScore, {
        fontSize: "48px",
        fill: "#fbb03b",
        stroke: "#000",
        strokeThickness: 8
    });
};

/**
Function that is called when the level is finished. It sets important variables back to their default values and starts the next level.
*/
levelTwo.prototype.changeLevel = function () {
    "use strict";
    pose = false;
    this.game.state.start("levelThree");
};

/**
Function adding a spike pit to the level. It needs a starting position and the spike count.
@param {int} xPosition - the x coordinate of the spike
@param {int} count - number of spikes
*/
levelTwo.prototype.addSpikePit = function (xPosition, count) {
    "use strict";
    spikes = death.create(xPosition, 515, "spikesleft");
    spikes.body.immovable = true;
    for (var i = 0; i < count; i++) {
        if (i % 2 === 0) {
            spikes = death.create(xPosition + 50 + i * 92, 515, "spikes");
        } else {
            spikes = death.create(xPosition + 50 + i * 92, 515, "spikesalt");
        }
        spikes.body.immovable = true;
    }
    spikes = death.create(xPosition + count * 92, 515, "spikesright");
    spikes.body.immovable = true;
};

/**
Function that adds a spinning sawblade to the level.
@param {int} xPosition - the x coordinate of the sawblade
@param {int} yPosition - the y coordinate of the sawblade
*/
levelTwo.prototype.addSawblade = function (xPosition, yPosition) {
    "use strict";
    var sawblade;
    sawblade = sawblades.create(xPosition, yPosition, "sawblade");
    sawblade.animations.add("rotate", [0, 1, 2, 3, 4, 5, 6], 30, true);
    sawblade.body.immovable = true;
    sawblade.body.width = 64;
    sawblade.body.height = 64;
    sawblade.body.x = 3;
    sawblade.body.y = 3;
};

/**
Function that adds a platform to the level.
@param {int} xPosition - the x coordinate of the platform
@param {int} yPosition - the y coordinate of the platform
@param {String} size - the size of the platform: "small", "medium", or "big"
@param {boolean} immovable - value that defines whether the platform is immovable or not
*/
levelTwo.prototype.addPlatform = function (xPosition, yPosition, size, immovable) {
    "use strict";
    switch (size) {
    case "small":
        var ledge = platforms.create(xPosition, yPosition, "platformsmall");
        ledge.body.setSize(110, 5, 20, 15);
        ledge.body.immovable = immovable;
        break;
    case "medium":
        var ledge2 = platforms.create(xPosition, yPosition, "platform");
        ledge2.body.setSize(200, 20, 20, 15);
        ledge2.body.immovable = immovable;
        break;
    case "big":
        var ledge3 = platforms.create(xPosition, yPosition, "platformbig");
        ledge3.body.setSize(320, 20, 20, 15);
        ledge3.body.immovable = immovable;
        break;
    }
};

/**
Function that adds a movable platform to the level. Just as in the function for the normal platforms,
this function also needs coordinates and a value for the size of the platform to be created.
HOWEVER, the platforms created in this function are different because they ae stored in a different array and need to be called in update() with the movePlatform()-method.
@param {int} xPosition - the x coordinate of the movable platform
@param {int} yPosition - the y coordinate of the movable platform
@param {String} size - the size of the  movable platform: "small", "medium", or "big"
*/
levelTwo.prototype.addMovablePlatform = function (xPosition, yPosition, size) {
    "use strict";
    switch (size) {
    case "small":
        var ledge = movablePlatforms.create(xPosition, yPosition, "platformsmall");
        ledge.body.setSize(110, 5, 20, 15);
        ledge.body.immovable = true;
        break;
    case "medium":
        var ledge2 = movablePlatforms.create(xPosition, yPosition, "platform");
        ledge2.body.setSize(200, 20, 20, 15);
        ledge2.body.immovable = true;
        break;
    case "big":
        var ledge3 = movablePlatforms.create(xPosition, yPosition, "platformbig");
        ledge3.body.setSize(320, 20, 20, 15);
        ledge3.body.immovable = true;
        break;
    }
};

/**
Function that adds a ground platform to the level. It needs an x coordinate and the corner which is supposed to be on the outside.
For instance, it only needs the right one at the start of the level since the left side is never shown on the screen.
@param {int} xPosition - the x coordinate of the ground platform
@param {String} corners - offers information on which side of the ground platform has a black outline
*/
levelTwo.prototype.addGround = function (xPosition, corners) {
    "use strict";
    switch (corners) {
    case "left":
        var ground = platforms.create(xPosition, this.game.world.height - 104, "groundleft");
        ground.body.immovable = true;
        break;
    case "middle":
        var ground1 = platforms.create(xPosition, this.game.world.height - 104, "ground");
        ground1.body.immovable = true;
        break;
    case "right":
        var ground2 = platforms.create(xPosition, this.game.world.height - 104, "groundright");
        ground2.body.immovable = true;
        break;
    case "single":
        var ground3 = platforms.create(xPosition, this.game.world.height - 104, "groundall");
        ground3.body.immovable = true;
        break;
    case "high":
        var ground4 = platforms.create(xPosition, this.game.world.height - 204, "groundhigh");
        ground4.body.immovable = true;
        break;
    case "higher":
        var ground5 = platforms.create(xPosition, this.game.world.height - 304, "groundhigh");
        ground5.body.immovable = true;
        break;
    }
};

/**
Function that adds a power up to the game.
@param {int} xPosition - the x coordinate of the power up
@param {int} yPosition - the y coordinate of the power up
@param {String} kindOfPowerUp - specifies the kind of power up, that is to say "shield" or "speed"
*/
levelTwo.prototype.addPowerUp = function (xPosition, yPosition, kindOfPowerUp) {
    "use strict";
    var powerUp = collectibles.create(xPosition, yPosition, kindOfPowerUp);
    powerUp.name = kindOfPowerUp;
};

/**
Function that adds a cluster of stars. It needs coordinates, the number of rows and columns, as well as a mode.
mode:    0 -> not diagonal
         1 -> diagonal ascending
         2 -> diagonal descending
@param {int} xPosition - the x coordinate of the star cluster
@param {int} yPosition - the y coordinate of the star cluster
@param {int} rows - number of rows
@param {int} cols - number of columns
@param {int} diagonal - information on the mode (see above)
*/
levelTwo.prototype.addStars = function (xPosition, yPosition, rows, cols, diagonal) {
    "use strict";
    var i, j;
    if (diagonal === 1) { //ascending
        for (i = 0; i < rows; i++) {
            collectibles.create(xPosition + i * 38, yPosition + i * 38, "star");
        }
    } else if (diagonal === 2) {
        for (i = 0; i < rows; i++) {
            collectibles.create(xPosition + i * 38, yPosition - i * 38, "star");
        }
    } else {
        for (i = 0; i < cols; i++) {
            for (j = 0; j < rows; j++) {
                collectibles.create(xPosition + j * 38, yPosition + i * 38, "star");
            }
        }
    }
};

/**
Function that adds a goal to the level.
@param {int} xPosition - the x coordinate of the goal
@param {int} yPosition - the y coordinate of the goal
*/
levelTwo.prototype.addGoal = function (xPosition, yPosition) {
    "use strict";
    goal = collectibles.create(xPosition, yPosition - 22, "goalnew");
    goal.frame = 1;
    goal.name = "goal";
};

/* Function that initiates the player sprite, gives him a body, adds animations, etc. */
levelTwo.prototype.initPlayer = function () {
    "use strict";
    player = this.game.add.sprite(32, this.game.world.height - 200, "character");
    this.game.physics.enable(player, Phaser.Physics.ARCADE);
    player.body.setSize(50, 64, 5, 0);
    player.body.gravity.y = gravity;
    player.body.collideWorldBounds = true;
    player.animations.add("left", [1, 2, 3, 4], 11, true);
    player.animations.add("right", [7, 8, 9, 10], 11, true);
    player.animations.add("shield_left", [16, 17, 18, 19], 11, true);
    player.animations.add("shield_right", [22, 23, 24, 25], 11, true);
    player.animations.add("speed_left", [30, 31, 32, 33], 11, true);
    player.animations.add("speed_right", [36, 37, 38, 39], 11, true);
    this.game.camera.follow(player);
    shield = false;
};

/* Function that initiates the different object groups. */
levelTwo.prototype.initGroups = function () {
    "use strict";
    death = this.game.add.group();
    death.enableBody = true;
    platforms = this.game.add.group();
    platforms.enableBody = true;
    sawblades = this.game.add.group();
    sawblades.enableBody = true;
    movablePlatforms = this.game.add.group();
    movablePlatforms.enableBody = true;
    collectibles = this.game.add.group();
    collectibles.enableBody = true;
    enemies = this.game.add.group();
    enemies.enableBody = true;
};

/* Checks for collisions of the player with objects in the game. */
levelTwo.prototype.updateCollisions = function () {
    "use strict";
    if (!died) {
        this.game.physics.arcade.collide(player, platforms);
        this.game.physics.arcade.collide(player, movablePlatforms);
        this.game.physics.arcade.overlap(player, goal, this.finishLevel, null, this);
        this.game.physics.arcade.collide(player, death, this.collision, null, this);
        this.game.physics.arcade.overlap(player, collectibles, this.collectCoin, null, this);
        this.game.physics.arcade.collide(player, sawblades, this.collision, null, this);
        this.game.physics.arcade.collide(player, enemies, this.collision, null, this);
    }
};

/* Function that updates the player movement. It is called in the update() function and checks for all kinds of different states, and shows the respective animations. */
levelTwo.prototype.updatePlayerMovement = function () {
    "use strict";

    if (!pose && !died) {
        cursors = this.game.input.keyboard.createCursorKeys();
        player.body.velocity.x = 0;
        if (cursors.left.isDown || pad1.isDown(Phaser.Gamepad.XBOX360_DPAD_LEFT) || pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) < -0.1) {
            player.body.velocity.x = -playerVelocityX;
            if (shield && !hitDeath) {
                player.animations.play("shield_left");
            } else if (powerupSpeed) {
                player.animations.play("speed_left");
            } else {
                player.animations.play("left");
            }

        } else if (cursors.right.isDown || pad1.isDown(Phaser.Gamepad.XBOX360_DPAD_RIGHT) || pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) > 0.1) {

            player.body.velocity.x = playerVelocityX;
            if (shield && !hitDeath) {
                player.animations.play("shield_right");
            } else if (powerupSpeed) {
                player.animations.play("speed_right");
            } else {
                player.animations.play("right");
            }

        } else {
            player.animations.stop();
            player.frame = 5;
            if (shield && !hitDeath) {
                player.frame = 20;
            } else if (powerupSpeed) {
                player.frame = 34;
            }
        }
        if (cursors.up.isDown && player.body.touching.down && !touchingCollectibles || pad1.justPressed(Phaser.Gamepad.XBOX360_A) && player.body.touching.down && !touchingCollectibles) {
            player.body.velocity.y = -playerVelocityY;
        }
        if (player.body.velocity.y < 0) {
            if (cursors.left.isDown || pad1.isDown(Phaser.Gamepad.XBOX360_DPAD_LEFT) || pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) < -0.1) {
                player.animations.stop();
                player.frame = 0;
                if (shield && !hitDeath) {
                    player.frame = 15;
                } else if (powerupSpeed) {
                    player.frame = 29;
                }
            }
            if (cursors.right.isDown || pad1.isDown(Phaser.Gamepad.XBOX360_DPAD_RIGHT) || pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) > 0.1) {
                player.animations.stop();
                player.frame = 11;
                if (shield && !hitDeath) {
                    player.frame = 26;
                } else if (powerupSpeed) {
                    player.frame = 40;
                }
            }
        }
    } else if (pose && !shield) {
        player.frame = 6;
    } else if (pose && shield && !hitDeath) {
        player.frame = 21;
    } else if (pose && powerupSpeed) {
        player.frame = 35;
    } else if (died && player.body.velocity.y < 0) {
        if (powerupSpeed) {
            player.frame = 41;
        } else {
            player.frame = 12;
        }
    } else if (powerupSpeed) {
        player.frame = 42;
    } else {
        player.frame = 13;
    }
    if (hitDeath) {
        player.animations.stop();
        player.frame = 14;
    }
};

/* Function that checks whether the user entered a cheat code. */
levelTwo.prototype.checkForCheats = function () {
    "use strict";
    if (this.game.input.keyboard.isDown(Phaser.Keyboard.G)) {
        playerVelocityX = 600;
        playerVelocityY = 600;
    } else if (this.game.input.keyboard.isDown(Phaser.Keyboard.K)) {
        playerVelocityX = 250;
        playerVelocityY = 350;
    }
};

/* Function that rotates the sawblades, that is to say that loads the respective animations. */
levelTwo.prototype.rotateSawblades = function () {
    "use strict";
    for (var i = 0; i < sawblades.children.length; i++) {
        sawblades.getChildAt(i).animations.play("rotate");
    }
};

/**
Function that starts the power up.
@param {int} num - specifies the number of the power up (0 = speed power up) -> identifier
*/
levelTwo.prototype.startPowerUp = function (num) {
    "use strict";
    switch (num) {
    case 0:
        playerVelocityX = 350;
        powerupSpeed = true;
        break;
    }
    this.game.time.events.add(Phaser.Timer.SECOND * 5, this.endPowerUp, this, num);
};

/**
Function that ends the power up.
@param {int} num - specifies the number of the power up (0 = speed power up) -> identifier
*/
levelTwo.prototype.endPowerUp = function (num) {
    "use strict";
    switch (num) {
    case 0:
        playerVelocityX = 250;
        playerVelocityY = 350;
        powerupSpeed = false;
        break;
    }
};

/* Function that is triggered when the user hits the ESC key, which then makes the game return to the title screen. */
levelTwo.prototype.exitLevel = function () {
    "use strict";
    if (this.game.input.keyboard.isDown(Phaser.Keyboard.ESC) || pad1.justPressed(Phaser.Gamepad.XBOX360_START)) {
        this.game.state.start("titleScreen");
    }
};
