var platforms,
    movablePlatforms,
    player,
    cursors,
    gravity,
    death,
    score,
    stars,
    starsText,
    starIcon,
    scoreText,
    timeText,
    collectibles,
    marginScore,
    goal,
    spikes,
    pose,
    died,
    sawblades,
    playerVelocityX,
    playerVelocityY,
    worldWidth,
    worldHeight,
    backgroundWidth,
    backgroundHeight,
    enemy,
    enemies,
    touchingCollectibles,
    highscoreCounter,
    pad1;

var levelOne = function () {
    "use strict";
    platforms = null;
    movablePlatforms = null;
    player = null;
    cursors = null;
    gravity = 600;
    death = null;
    score = 0;
    stars = 0;
    starsText = null;
    starIcon = null;
    scoreText = null;
    timeText = null;
    collectibles = null;
    marginScore = 16;
    goal = null;
    spikes = null;
    pose = false;
    died = false;
    sawblades = null;
    playerVelocityX = 250;
    playerVelocityY = 350;
    worldWidth = 4800;
    worldHeight = 600;
    backgroundWidth = 800;
    backgroundHeight = 600;
    enemy = null;
    enemies = null;
    touchingCollectibles = false;
    highscoreCounter = 0;
    pad1 = null;
};

levelOne.prototype = {
    /* global Phaser
    global sessionStorage
    global localStorage */
    /* Function that loads all the assets, adds arcade physics, and initializes the groups as well as the UI. */
    create: function () {
        "use strict";
        died= false;
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.game.world.setBounds(0, 0, worldWidth, worldHeight);

        this.createBackground();
        this.initUI();
        this.initGroups();

        this.addGround(-75, "right");
        this.addGround(2000, "single");
        this.addGround(2800, "single");
        this.addGround(4280, "single");

        this.addPlatform(295, 400, "medium", true);
        this.addPlatform(550, 270, "medium", true);
        this.addPlatform(800, 400, "big", true);
        this.addPlatform(1250, 333, "small", true);
        this.addPlatform(1500, 300, "big", true);
        this.addPlatform(2040, 270, "medium", false);
        this.addPlatform(2400, 400, "medium", false);

        this.addMovablePlatform(3200, 150, "medium");
        this.addMovablePlatform(3600, 300, "medium");

        this.addStars(340, 360, 4, 1, 0);
        this.addStars(620, 170, 2, 2, 0);
        this.addStars(800, 170, 4, 1, 1);
        this.addStars(1250, 275, 3, 1, 2);
        this.addStars(2080, 150, 4, 3, 0);
        this.addStars(1650, 130, 2, 2, 0);
        this.addStars(2475, 250, 2, 2, 0);
        this.addStars(2975, 100, 1, 3, 0);
        this.addStars(3100, 325, 2, 1, 2);
        this.addStars(3300, 450, 1, 1, 0);
        this.addStars(3484, 125, 1, 1, 0);
        this.addStars(4200, 300, 2, 1, 1);

        this.addSawblade(810, 350);
        this.addSawblade(1025, 350);
        this.addSawblade(3465, 190);
        this.addSawblade(3465, 260);
        this.addSawblade(3465, 330);

        this.addSpikePit(280, 18);
        this.addSpikePit(2288, 5);
        this.addSpikePit(3158, 12);
        this.addSpikePit(4630, 3);

        this.addGoal(4500, 460);

        this.initPlayer();

        this.game.time.events.add(Phaser.Timer.MINUTE * 2.5, this.collision, this);
        touchingCollectibles = false;
        score = 0;
        stars = 0;
        this.game.input.gamepad.start();
        pad1 = this.game.input.gamepad.pad1;

    },

    /* Function that is repeatedly called when the game is running. It is responsible for showing animations of the assets as well as the character.
    Moreover, it checks for cheats, updates the UI, and tests whether the character collides with enemies, collectibles, or traps. */
    update: function () {
        "use strict";
        if (!this.game.isPaused) {

            this.game.background.position.x = -(this.game.camera.x * 0.001);

            this.updateCollisions();

            this.checkForCheats();

            this.rotateSawblades();

            this.updatePlayerMovement();
            this.exitLevel();
            timeText.text = this.millisToMinutesAndSeconds(this.game.time.events.duration);

            this.movePlatform(0, "y", 300, 150, 1);
            this.movePlatform(1, "x", 3750, 3200, 1);

        }
    }
};

/* Creates the background according to the width and the height defined above. */
levelOne.prototype.createBackground = function () {
    "use strict";
    for (var i = 0; i < (worldWidth / backgroundHeight).toFixed(0); i++) {
        this.game.background = this.game.add.tileSprite(0, 0, backgroundWidth + i * backgroundWidth, backgroundHeight, "bg");
    }
    this.game.background.fixedToCamera = false;
};

/* Initializes the user interface, that is to say the texts and icons that inform the user about the time and the collectibles he has collected. */
levelOne.prototype.initUI = function () {
    "use strict";
    scoreText = this.game.add.text(marginScore, marginScore, "000000", {
        fontSize: "24px",
        fill: "#FFF",
        stroke: "#000",
        strokeThickness: 8
    });
    scoreText.fixedToCamera = true;

    starsText = this.game.add.text(350, marginScore, "000", {
        fontSize: "24px",
        fill: "#FFF",
        stroke: "#000",
        strokeThickness: 8
    });
    starsText.fixedToCamera = true;

    starIcon = this.game.add.sprite(410, marginScore + 5, "star");
    starIcon.scale.setTo(0.8, 0.8);
    starIcon.fixedToCamera = true;

    timeText = this.game.add.text(700, marginScore, "3:00", {
        fontSize: "24px",
        fill: "#FFF",
        stroke: "#000",
        strokeThickness: 8
    });
    timeText.fixedToCamera = true;

};

/* Animation that is called when the user has either touched an enemy or a trap. After the animation has been played, the level is restarted. */
levelOne.prototype.collision = function () {
    "use strict";
    died = true;
    player.body.collideWorldBounds = false;
    player.body.velocity.x = 0;
    player.body.velocity.y = 0;
    player.body.velocity.y = -400;
    this.game.time.events.add(Phaser.Timer.SECOND * 2.5, this.restartGame, this);
};

/* Restarts the game by resetting all significant variables to their default value. */
levelOne.prototype.restartGame = function () {
    "use strict";
    this.game.world.camera.x = 0;
    score = 0;
    stars = 0;
    pose = false;
    died = false;
    this.game.isPaused = false;
    this.game.state.start(this.game.state.current);
};

/**
Adds an enemy to the game including left and right walking animations.
@param {int} xPosition - the enemy's x coordniate
@param {int} yPosition - the enemy's y coordniate
*/
levelOne.prototype.addEnemy = function (xPosition, yPosition) {
    "use strict";
    enemy = enemies.create(xPosition, yPosition, "enemy");
    enemy.body.immovable = true;
    enemy.animations.add("left", [0, 1], 5, true);
    enemy.animations.add("right", [3, 4], 5, true);
};

/**
Function that moves one of the movable platforms. It needs the number of the platform (starting at 0), its direction (either "x" or "y"),
its border (at which it will turn the other way), and its speed.
@param {int} number - number of the platform
@param {String} direction - direction in which the platform moves
@param {int} border - the coordinate value at which the platform stops to move and turns the other way
@param {int} start - the coordinate value where the platform starts
@param {int} speed - the platform's speed
*/
levelOne.prototype.movePlatform = function (number, direction, border, start, speed) {
    "use strict";
    var platform = movablePlatforms.getAt(number);
    switch (direction) {
    case "x":
        if (platform.x < border) {
            platform.body.velocity.x += speed;
        } else {
            platform.body.velocity.x += -speed;
        }
        break;
    case "y":
        if (platform.y < border) {
            platform.body.velocity.y += speed;
        } else {
            platform.body.velocity.y += -speed;
        }
    }
};

/**
Function which makes enemies move.
@param {int} number - number of the enemy
@param {int} border - the coordinate value at which the enemy stops to move and turns the other way
@param {int} speed - the enemy's speed
*/
levelOne.prototype.moveEnemies = function (number, border, speed) {
    "use strict";
    var enemyCurrent = enemies.getAt(number);
    if (enemyCurrent.x < border) {
        enemyCurrent.body.velocity.x += speed;
    } else {
        enemyCurrent.body.velocity.x += -speed;
    }
    if (enemyCurrent.body.velocity.x < 0) {
        enemyCurrent.animations.play("left");
    } else {
        enemyCurrent.animations.play("right");
    }
};

/* Function that sets touchingCollectibles back to false which gives the character the ability to jump again, right after having collected a collectible. */
levelOne.prototype.setTouchingCollectiblesBackToFalse = function () {
    "use strict";
    touchingCollectibles = false;
};

/**
Function that is called when the player collects a collectible. It checks whether the collectible is a star or the goal. In the former case,
it updates the UI respectively and kills the coin. In the latter, this function does nothing.
By setting touchingCollectibles to true, this function keeps the character from being able to jump when touching the collectibles.
@param {Object} playerCurrent - the character touching the collectible
@param {Object} coin - the collectible touched by the character
*/
levelOne.prototype.collectCoin = function (playerCurrent, coin) {
    "use strict";
    touchingCollectibles = true;
    if (coin.name !== "goal") {
        coin.kill();
        stars++;
        starsText.text = this.pad(stars, 3);
        score += 10;
        scoreText.text = this.pad(score, 6);
        this.game.time.events.add(Phaser.Timer.SECOND * 0.001, this.setTouchingCollectiblesBackToFalse, this);
    }
};

/**
Function that helps with the calculation of the score.
@param {int} num - number of stars or score respectively
@param {int} size - the number of digits being displayed in the UI
@return {String} - the respective UI element
*/
levelOne.prototype.pad = function (num, size) {
    "use strict";
    var s = "000000" + num;
    return s.substr(s.length - size);
};

/**
Function that formats the timer.
@param {int} millis - milliseconds which need to be formated
@return {String} - the respective UI element
*/
levelOne.prototype.millisToMinutesAndSeconds = function (millis) {
    "use strict";
    var minutes = Math.floor(millis / 60000);
    var seconds = ((millis % 60000) / 1000).toFixed(0);
    return minutes + ":" + (seconds < 10 ? "0" : "") + seconds;
};

/* Function that is called when the user finishes the level, it sets explicit varaibles to the needed values, such as pose to true,
which triggers the respective pose animation. It also calls the function which calculates the highscore. */
levelOne.prototype.finishLevel = function () {
    "use strict";
    player.body.velocity.x = 0;
    pose = true;
    goal.frame = 0;
    this.game.time.events.add(Phaser.Timer.SECOND * 5, this.changeLevel, this);
    // hier Infos für Highscore abfragen
    if (highscoreCounter === 0) {
        this.setHighscore(scoreText.text, timeText.text);
        highscoreCounter++;
    }
};

/** Function that calculates the highscore and saves the user's current score to the browser's localStorage object.
@param {String} scoreCurrent - the user's final score after having finished the level
@param {String} time - the time the user need to complete the level
*/
levelOne.prototype.setHighscore = function (scoreCurrent, time) {
    "use strict";
    var highScoreObject,
        username;
    time = (parseInt(time.substring(0, 1)) * 60) + parseInt(time.substring(2));
    username = sessionStorage.getItem("username");
    highScoreObject = {
        "time": time,
        "username": username,
        "score": scoreCurrent,
        "accumulatedScore": parseInt(scoreCurrent) + time,
        "level": 1
    };
    this.sendScoreToScreen(highScoreObject);
    highScoreObject = JSON.stringify(highScoreObject);
    localStorage.setItem("highScoreEntry" + localStorage.length, highScoreObject);
};

/**
Function that sends information about the user's score to screen. It displays the number of stars the user collected, the time left, and the accumulated score.
@param {Object} highScoreObject - object that contains all the important score information
*/
levelOne.prototype.sendScoreToScreen = function (highScoreObject) {
    "use strict";
    this.game.add.image(this.game.camera.x + 200, 100, "highScoreLevelEnd");
    this.game.add.text(this.game.camera.x + 350, 176, (String(highScoreObject.score)).substring(3, 5), {
        fontSize: "48px",
        fill: "#fbb03b",
        stroke: "#000",
        strokeThickness: 8
    });
    this.game.add.text(this.game.camera.x + 350, 246, highScoreObject.time, {
        fontSize: "48px",
        fill: "#fbb03b",
        stroke: "#000",
        strokeThickness: 8
    });
    this.game.add.text(this.game.camera.x + 350, 316, highScoreObject.accumulatedScore, {
        fontSize: "48px",
        fill: "#fbb03b",
        stroke: "#000",
        strokeThickness: 8
    });
};

/**
Function that is called when the level is finished. It sets important variables back to their default values and starts the next level.
*/
levelOne.prototype.changeLevel = function () {
    "use strict";
    pose = false;
    touchingCollectibles = false;
    stars = 0;
    score = 0;
    this.game.state.start("levelTwo");
};

/**
Function adding a spike pit to the level. It needs a starting position and the spike count.
@param {int} xPosition - the x coordinate of the spike
@param {int} count - number of spikes
*/
levelOne.prototype.addSpikePit = function (xPosition, count) {
    "use strict";
    spikes = death.create(xPosition, 515, "spikesleft");
    spikes.body.immovable = true;
    for (var i = 0; i < count; i++) {
        if (i % 2 === 0) {
            spikes = death.create(xPosition + 50 + i * 92, 515, "spikes");
            spikes.body.immovable = true;
        } else {
            spikes = death.create(xPosition + 50 + i * 92, 515, "spikesalt");
            spikes.body.immovable = true;
        }
    }
    spikes = death.create(xPosition + count * 92, 515, "spikesright");
    spikes.body.immovable = true;
};

/**
Function that adds a spinning sawblade to the level.
@param {int} xPosition - the x coordinate of the sawblade
@param {int} yPosition - the y coordinate of the sawblade
*/
levelOne.prototype.addSawblade = function (xPosition, yPosition) {
    "use strict";
    var sawblade;
    sawblade = sawblades.create(xPosition, yPosition, "sawblade");
    sawblade.animations.add("rotate", [0, 1, 2, 3, 4, 5, 6], 30, true);
    sawblade.body.immovable = true;
    sawblade.body.width = 64;
    sawblade.body.height = 64;
    sawblade.body.x = 3;
    sawblade.body.y = 3;
};

/**
Function that adds a platform to the level.
@param {int} xPosition - the x coordinate of the platform
@param {int} yPosition - the y coordinate of the platform
@param {String} size - the size of the platform: "small", "medium", or "big"
@param {boolean} immovable - value that defines whether the platform is immovable or not
*/
levelOne.prototype.addPlatform = function (xPosition, yPosition, size, immovable) {
    "use strict";
    switch (size) {
    case "small":
        var ledge = platforms.create(xPosition, yPosition, "platformsmall");
        ledge.body.setSize(110, 5, 20, 15);
        ledge.body.immovable = immovable;
        break;
    case "medium":
        var ledge2 = platforms.create(xPosition, yPosition, "platform");
        ledge2.body.setSize(200, 20, 20, 15);
        ledge2.body.immovable = immovable;
        break;
    case "big":
        var ledge3 = platforms.create(xPosition, yPosition, "platformbig");
        ledge3.body.setSize(320, 20, 20, 15);
        ledge3.body.immovable = immovable;
        break;
    }
};

/**
Function that adds a movable platform to the level. Just as in the function for the normal platforms,
this function also needs coordinates and a value for the size of the platform to be created.
HOWEVER, the platforms created in this function are different because they ae stored in a different array and need to be called in update() with the movePlatform()-method.
@param {int} xPosition - the x coordinate of the movable platform
@param {int} yPosition - the y coordinate of the movable platform
@param {String} size - the size of the  movable platform: "small", "medium", or "big"
*/
levelOne.prototype.addMovablePlatform = function (xPosition, yPosition, size) {
    "use strict";
    switch (size) {
    case "small":
        var ledge = movablePlatforms.create(xPosition, yPosition, "platformsmall");
        ledge.body.setSize(110, 5, 20, 15);
        ledge.body.immovable = true;
        break;
    case "medium":
        var ledge2 = movablePlatforms.create(xPosition, yPosition, "platform");
        ledge2.body.setSize(200, 20, 20, 15);
        ledge2.body.immovable = true;
        break;
    case "big":
        var ledge3 = movablePlatforms.create(xPosition, yPosition, "platformbig");
        ledge3.body.setSize(320, 20, 20, 15);
        ledge3.body.immovable = true;
        break;
    }
};


/**
Function that adds a ground platform to the level. It needs an x coordinate and the corner which is supposed to be on the outside.
For instance, it only needs the right one at the start of the level since the left side is never shown on the screen.
@param {int} xPosition - the x coordinate of the ground platform
@param {String} corners - offers information on which side of the ground platform has a black outline
*/
levelOne.prototype.addGround = function (xPosition, corners) {
    "use strict";
    switch (corners) {
    case "left":
        var ground = platforms.create(xPosition, worldHeight - 104, "groundleft");
        ground.body.immovable = true;
        break;
    case "middle":
        var ground1 = platforms.create(xPosition, worldHeight - 104, "ground");
        ground1.body.immovable = true;
        break;
    case "right":
        var ground2 = platforms.create(xPosition, worldHeight - 104, "groundright");
        ground2.body.immovable = true;
        break;
    case "single":
        var ground3 = platforms.create(xPosition, worldHeight - 104, "groundall");
        ground3.body.immovable = true;
        break;
    }
};

/**
Function that adds a cluster of stars. It needs coordinates, the number of rows and columns, as well as a mode.
mode:    0 -> not diagonal
         1 -> diagonal ascending
         2 -> diagonal descending
@param {int} xPosition - the x coordinate of the star cluster
@param {int} yPosition - the y coordinate of the star cluster
@param {int} rows - number of rows
@param {int} cols - number of columns
@param {int} diagonal - information on the mode (see above)
*/
levelOne.prototype.addStars = function (xPosition, yPosition, rows, cols, diagonal) {
    "use strict";
    var i,
        j;
    if (diagonal === 1) { //ascending
        for (i = 0; i < rows; i++) {
            collectibles.create(xPosition + i * 38, yPosition + i * 38, "star");
        }
    } else if (diagonal === 2) {
        for (i = 0; i < rows; i++) {
            collectibles.create(xPosition + i * 38, yPosition - i * 38, "star");
        }
    } else {
        for (i = 0; i < cols; i++) {
            for (j = 0; j < rows; j++) {
                collectibles.create(xPosition + j * 38, yPosition + i * 38, "star");
            }
        }
    }
};

/**
Function that adds a goal to the level.
@param {int} xPosition - the x coordinate of the goal
@param {int} yPosition - the y coordinate of the goal
*/
levelOne.prototype.addGoal = function (xPosition, yPosition) {
    "use strict";
    goal = collectibles.create(xPosition, yPosition - 22, "goalnew");
    goal.frame = 1;
    goal.name = "goal";
};

/* Function that initiates the player sprite, gives him a body, adds animations, etc. */
levelOne.prototype.initPlayer = function () {
    "use strict";
    player = this.game.add.sprite(32, worldHeight - 200, "character");
    this.game.physics.enable(player, Phaser.Physics.ARCADE);
    player.body.setSize(50, 64, 5, 0);
    player.body.gravity.y = gravity;
    player.body.collideWorldBounds = true;
    player.animations.add("left", [1, 2, 3, 4], 11, true);
    player.animations.add("right", [7, 8, 9, 10], 11, true);
    this.game.camera.follow(player);
};

/* Function that initiates the different object groups. */
levelOne.prototype.initGroups = function () {
    "use strict";
    death = this.game.add.group();
    death.enableBody = true;
    platforms = this.game.add.group();
    platforms.enableBody = true;
    sawblades = this.game.add.group();
    sawblades.enableBody = true;
    movablePlatforms = this.game.add.group();
    movablePlatforms.enableBody = true;
    collectibles = this.game.add.group();
    collectibles.enableBody = true;
    enemies = this.game.add.group();
    enemies.enableBody = true;
};

/* Checks for collisions of the player with objects in the game. */
levelOne.prototype.updateCollisions = function () {
    "use strict";
    if (!died) {
        this.game.physics.arcade.collide(player, platforms);
        this.game.physics.arcade.collide(player, movablePlatforms);
        this.game.physics.arcade.overlap(player, goal, this.finishLevel, null, this);
        this.game.physics.arcade.collide(player, death, this.collision, null, this);
        this.game.physics.arcade.overlap(player, collectibles, this.collectCoin, null, this);
        this.game.physics.arcade.collide(player, sawblades, this.collision, null, this);
        this.game.physics.arcade.collide(player, enemies, this.collision, null, this);
    }
};

/* Function that updates the player movement. It is called in the update() function and checks for all kinds of different states, and shows the respective animations. */
levelOne.prototype.updatePlayerMovement = function () {
    "use strict";
    if (!pose && !died) {
        cursors = this.game.input.keyboard.createCursorKeys();
        player.body.velocity.x = 0;
        if (cursors.left.isDown || pad1.isDown(Phaser.Gamepad.XBOX360_DPAD_LEFT) || pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) < -0.1) {
            player.body.velocity.x = -playerVelocityX;
            player.animations.play("left");
        } else if (cursors.right.isDown || pad1.isDown(Phaser.Gamepad.XBOX360_DPAD_RIGHT) || pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) > 0.1) {
            player.body.velocity.x = playerVelocityX;
            player.animations.play("right");
        } else {
            player.animations.stop();
            player.frame = 5;
        }
        if (cursors.up.isDown && player.body.touching.down && !touchingCollectibles || pad1.justPressed(Phaser.Gamepad.XBOX360_A) && player.body.touching.down && !touchingCollectibles) {
            player.body.velocity.y = -playerVelocityY;
        }
        if (player.body.velocity.y < 0) {
            if (cursors.left.isDown || pad1.isDown(Phaser.Gamepad.XBOX360_DPAD_LEFT) || pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) < -0.1) {
                player.animations.stop();
                player.frame = 0;
            }
            if (cursors.right.isDown || pad1.isDown(Phaser.Gamepad.XBOX360_DPAD_RIGHT) || pad1.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) > 0.1) {
                player.animations.stop();
                player.frame = 11;
            }
        }
    } else if (pose) {
        player.frame = 6;
    } else if (died && player.body.velocity.y < 0) {
        player.frame = 12;
    } else {
        player.frame = 13;
    }
};

/* Function that checks whether the user entered a cheat code. */
levelOne.prototype.checkForCheats = function () {
    "use strict";
    if (this.game.input.keyboard.isDown(Phaser.Keyboard.G)) {
        playerVelocityX = 600;
        playerVelocityY = 600;
    } else if (this.game.input.keyboard.isDown(Phaser.Keyboard.K)) {
        playerVelocityX = 250;
        playerVelocityY = 350;
    }
};

/* Function that rotates the sawblades, that is to say that loads the respective animations. */
levelOne.prototype.rotateSawblades = function () {
    "use strict";
    for (var i = 0; i < sawblades.children.length; i++) {
        sawblades.getChildAt(i).animations.play("rotate");
    }
};

/* Function that is triggered when the user hits the ESC key, which then makes the game return to the title screen. */
levelOne.prototype.exitLevel = function () {
    "use strict";
    if (this.game.input.keyboard.isDown(Phaser.Keyboard.ESC) || pad1.justPressed(Phaser.Gamepad.XBOX360_START)) {
        this.game.state.start("titleScreen");
    }
};
